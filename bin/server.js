const http = require('http');
const app = require('../src/app');
const log = require('../lib/logger');

function createServerErrorHandler(hostWithPort) {
  return (error) => {
    if (error.syscall !== 'listen') {
      throw error;
    }

    switch (error.code) {
      case 'EACCES':
        log.error(`${hostWithPort} requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        log.error(`${hostWithPort} is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  };
}

function createListeningHandler(hostWithPort) {
  return () => {
    log.info(`Listening on ${hostWithPort}`);
  };
}

async function run() {
  const server = http.createServer(await app());
  const host = process.env.APP_HOST;
  const port = process.env.APP_PORT;
  const hostWithPort = `${host}:${port}`;

  server.listen(port, host);
  server.on('error', createServerErrorHandler(hostWithPort));
  server.on('listening', createListeningHandler(hostWithPort));
}

run();
