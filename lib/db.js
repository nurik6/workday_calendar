const { MongoClient } = require('mongodb');
const log = require('./logger');

module.exports = async () => {
  let db;
  let client;
  try {
    client = await MongoClient.connect(process.env.MONGODB_URL, { useNewUrlParser: true });
    db = await client.db(process.env.MONGODB_DBNAME);
    log.info('Mongo connection success.');
  } catch (err) {
    log.error(err);
  }
  client.on('close', () => {
    log.info('Mongo connection closed');
  });
  return { db, client };
};
