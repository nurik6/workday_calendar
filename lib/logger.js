const bunyan = require('bunyan');

const log = bunyan.createLogger({ name: 'working_days_calendar' });
module.exports = log;
