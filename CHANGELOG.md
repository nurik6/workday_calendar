# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.0"></a>
# 1.1.0 (2018-08-19)


### Bug Fixes

* **app.js:** Moved to src dir ([b1d2c7f](https://bitbucket.org/nurik6/workday_calendar/commits/b1d2c7f))
* **create_collections.sh:** bin/create_collections.sh ([4370485](https://bitbucket.org/nurik6/workday_calendar/commits/4370485))
* **db:** added useNewUrlParser: true ([1d761bc](https://bitbucket.org/nurik6/workday_calendar/commits/1d761bc))
* **docker-compose:** serrvice renamed  mongo -> mongodb ([21de78b](https://bitbucket.org/nurik6/workday_calendar/commits/21de78b))
* **events:**  Rewrited, and optimized stats, list methods in order to serve huge amount of data ([9c75fd9](https://bitbucket.org/nurik6/workday_calendar/commits/9c75fd9))
* **events/list:** Stop and close mongo stream, if connection aborted ([81335ba](https://bitbucket.org/nurik6/workday_calendar/commits/81335ba))
* **npm start|start-dev:** Fix path to start script ([10e84ac](https://bitbucket.org/nurik6/workday_calendar/commits/10e84ac))
* **orderredlist:** removed ** from list items ([9a6b5c7](https://bitbucket.org/nurik6/workday_calendar/commits/9a6b5c7))
* **packages:** added migrate command, removed createCollections ([d7c6a16](https://bitbucket.org/nurik6/workday_calendar/commits/d7c6a16))
* project renamed ([05735e6](https://bitbucket.org/nurik6/workday_calendar/commits/05735e6))
* **README:** added additional info in install section, renamed ([82485f1](https://bitbucket.org/nurik6/workday_calendar/commits/82485f1))
* **README:** fixed some styles ([d41c3e7](https://bitbucket.org/nurik6/workday_calendar/commits/d41c3e7))
* **README:** removed last command from install section ([f39c1f5](https://bitbucket.org/nurik6/workday_calendar/commits/f39c1f5))
* **server.js:** Fix listen port and host ([72abfd3](https://bitbucket.org/nurik6/workday_calendar/commits/72abfd3))


### Features

* **attachValidateMethod:** Added ([24dfa83](https://bitbucket.org/nurik6/workday_calendar/commits/24dfa83))
* **compose file:** run coommand replaced with real app ([d2d4e3e](https://bitbucket.org/nurik6/workday_calendar/commits/d2d4e3e))
* **docker-compose:** Get exposed app ports from .env ([a7ec640](https://bitbucket.org/nurik6/workday_calendar/commits/a7ec640))
* **docker-compose:** run command replaced with RUN_COMMAND ([e8679ae](https://bitbucket.org/nurik6/workday_calendar/commits/e8679ae))
* **dotenv:** Added ([a3f3db5](https://bitbucket.org/nurik6/workday_calendar/commits/a3f3db5))
* **employees:** Added routes, controller ([fde4758](https://bitbucket.org/nurik6/workday_calendar/commits/fde4758))
* **EmployeeService,errors,validators:** Added ([bbcbd97](https://bitbucket.org/nurik6/workday_calendar/commits/bbcbd97))
* **envs, code format:** Added ([1aa8289](https://bitbucket.org/nurik6/workday_calendar/commits/1aa8289))
* **logger:** bunyan logger -> lib dir ([4a7a97d](https://bitbucket.org/nurik6/workday_calendar/commits/4a7a97d))
* **middlewares,errors,handlers:** Added ([def02f4](https://bitbucket.org/nurik6/workday_calendar/commits/def02f4))
* **mongodb:** Added db module -> lib/db.js ([cfc0d8f](https://bitbucket.org/nurik6/workday_calendar/commits/cfc0d8f))
* **package.json:**  Added JSONStream ([0898480](https://bitbucket.org/nurik6/workday_calendar/commits/0898480))
* **package.json:** Added create_collections command ([87c5a97](https://bitbucket.org/nurik6/workday_calendar/commits/87c5a97))
* **packages:** Added new packages ([2fcfd7d](https://bitbucket.org/nurik6/workday_calendar/commits/2fcfd7d))
* **README:** Added ([858cd11](https://bitbucket.org/nurik6/workday_calendar/commits/858cd11))
* **README:** Added additinal notes ([465ee8a](https://bitbucket.org/nurik6/workday_calendar/commits/465ee8a))
* **README:** Added new RUN_COMMAND env ([3b6c5bc](https://bitbucket.org/nurik6/workday_calendar/commits/3b6c5bc))
* **README:** Added notes about task 4 ([0826321](https://bitbucket.org/nurik6/workday_calendar/commits/0826321))
* **routes:** Added routes loader, and helper ([6a8bb47](https://bitbucket.org/nurik6/workday_calendar/commits/6a8bb47))
* **server.js:** Added server.js as start script ([407c33c](https://bitbucket.org/nurik6/workday_calendar/commits/407c33c))
* **standard-version:** Added dev tool, to use releases ([b50fa9a](https://bitbucket.org/nurik6/workday_calendar/commits/b50fa9a))
* **start-dev:** human readable logs for dev ([b8269d9](https://bitbucket.org/nurik6/workday_calendar/commits/b8269d9))
* **vscode:** Added debugger connection info ([8f3b75e](https://bitbucket.org/nurik6/workday_calendar/commits/8f3b75e))
