require('dotenv').load();

module.exports = {
  host: 'mongodb',
  db: process.env.MONGODB_DBNAME,
  directory: './migrations',
};
