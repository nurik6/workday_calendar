# Workday Calendar

----
## install
* `git glone https://bitbucket.org/nurik6/workday_calendar`
* `cp .env ./workday_calendar`
* `cd workdir_calendar && docker-compose up`
* ~~`./bin/create_collections.sh`~~
* `docker exec -it $(docker ps | grep app | awk '{ print $1}') npm run migrate`

## ENV Params
* `APP_PORT=8001` 
* `APP_HOST=0.0.0.0`
* `APP_DEBUG_PORT=9229`
* `MONGODB_URL=mongodb://mongodb:27017`
* `MONGODB_DBNAME=calendar`
* `TRACE=1 #use when you want get more debug info`
* `NODE_ENV=development`
* `RUN_COMMAND=start # valid values are start or start-dev`
* `WORK_DAY_DURATION=8 # set work day duration`
* `DATE_FORMAT=DD.MM.YY`
* `# 1 - ASC, -1 - DESC`
* `DEFAULT_EVENTS_SORT_ORDER=1`

If you see errors messages please make sure, that .env is in root of project

----
## Work with employees

* GET /employee/<ObjectId>
* GET /employees
* POST /employee (JSON Body)
* PUT /employee/<ObjectId>
* DELETE /employee/<ObjectId>

----
## Work with calendar events

* GET /events : list of events
* GET /event/<ObjectId> return event object by id
* POST /event - create new event
* PUT /event/<ObjectId> modify event
* DELETE /employee/<ObjectId> - delete event
* GET /events/stats - returns statistic about all events in given range
from and to params is required

## Результаты задания:
Помимо реализованного сервиса, пожалуйста, ответьте на следующие вопросы:

1. где-то 17 часов (из низ большее время потратил на mongodb, в основном работаю с реляционными БД)
2. где-то 5-6 лет. Из них обдуманно и серьезно последние 4 года. На ноде пишу 2.5 года (+ полгода написания всяких скриптов).
3. Опыт работы с СУБД более 3 лет. Запросы различные, типа n-per-group и так далее.
4. Опыт работы с фронтендом небольшой. Всё ограничивалось установкой twitter bootstrap. Но верстать немного умею.
 
*Примечание:* Не успел выполнить 5 последнее дополнительное задание.
Но думал, читать из стрима запроса и писать это в mongo с помощью создания mongo курсора и запайпить это всё с помощью highlandjs 


Задание 4 - ~~Не уверен что правильно понял задачу. Сделал так: Установил highland, отдаю в методе GET /events данные порционно, в стрим респонса, чтобы не грузить БД.~~
<br>Удалил highland, убрал лишний коннект, создаваемый с помощью ненужного mongojs,
запайпил курсор AggregateCursor на response, добавил параметры для запроса mongo: `allowDiskUse: true, batchSize: 10000`, добавил обработку ошибок, а также принудительное завершение стрима, на случай, если оборвался TCP коннект запроса.

* Тесты тоже отсутствуют, но планирую покрыть все методы тестами, в течении близжайших 2-3 дней. Как освобожусь. 

* В связи с тем, что MongoDB не часто применяю, слой хранения данных перемешен в сервисах. Т.е. SRP принцип из SOLID нарушен.  Писал его, после рабочего времени. Последний год работал с проектом на koa2. Поэтому немного времени заняла подготовка бойлерплейта + на работе мы больше используем реляционные субд в качестве основного хранилища.

* Не использовал стандартный git-flow, потому что, нужно было быстро пилить фичи. Но представление о работе с git-flow имеются и  применяются.