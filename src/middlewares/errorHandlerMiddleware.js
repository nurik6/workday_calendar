const ErrorResource = require('../resources/ErrorResource');
const log = require('../../lib/logger');

module.exports = async (err, req, res, next) => {
  if (!(err.status) || !(err.code)) {
    res.status(500);
    log.error(err);
  } else {
    res.status(err.status);
    err.setErrorNo(req.headers['X-Request-ID']);
  }
  return res.send(new ErrorResource(err));
};
