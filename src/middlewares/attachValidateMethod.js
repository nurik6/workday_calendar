const { validationResult } = require('express-validator/check');
const ValidationError = require('../errors/ValidationError');

module.exports = async (req, res, next) => {
  function validate() {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw new ValidationError('Validation error', errors.mapped());
    }
  }
  req.validate = validate;
  next();
};
