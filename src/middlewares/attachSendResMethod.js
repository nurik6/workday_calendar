module.exports = async (req, res, next) => {
  function sendRes(data) {
    return res.send({
      code: 0,
      name: 'OK',
      data,
    });
  }
  res.status(200);
  res.sendRes = sendRes;
  next();
};
