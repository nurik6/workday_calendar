const uuidv4 = require('uuid/v4');

module.exports = async (req, res, next) => {
  req.headers['X-Request-ID'] = uuidv4();
  return next();
};
