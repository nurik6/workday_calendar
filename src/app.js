require('dotenv').load();
const expressValidatorMiddleware = require('express-validator');
const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('../lib/db');
// const log = require('../lib/logger');
const routes = require('./routes');
// Common middlewares
const notFoundMiddleware = require('./middlewares/notFoundMiddleware');
const errorHandlerMiddleware = require('./middlewares/errorHandlerMiddleware');
const attachRequestIdMiddleware = require('./middlewares/attachRequestIdMiddleware');
const attachSendResMethod = require('./middlewares/attachSendResMethod');
const attachValidateMethod = require('./middlewares/attachValidateMethod');

module.exports = async () => {
  const { db } = await mongodb();
  const app = express();
  // Set data sources
  app.set('db', db);
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));
  // parse application/json
  app.use(bodyParser.json());
  app.use(attachSendResMethod);
  app.use(expressValidatorMiddleware());
  // Attach routes to app
  app.use(attachValidateMethod);
  routes(app);
  app.use(attachRequestIdMiddleware);
  app.use(notFoundMiddleware);
  app.use(errorHandlerMiddleware);
  return app;
};
