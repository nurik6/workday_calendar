module.exports.FilterSet = class FilterSet {
  constructor() {
    this.filters = [];
  }

  add(filter) {
    this.filters.push(filter);
  }

  get() {
    return this
      .filters.filter(rule => (rule.get() !== null))
      .reduce((acc, value) => Object.assign(acc, value.get()), {});
  }
};
