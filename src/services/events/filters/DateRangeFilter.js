module.exports = class DateRangeFilter {
  constructor({ from = null, to = null }) {
    this.from = from;
    this.to = to;
  }

  get() {
    if (this.from !== null && this.to === null) {
      return { date: this.from };
    } else if (this.from !== null && this.to !== null) {
      return { date: { $gte: this.from, $lte: this.to }};
    } else {
      return null;
    }
  }
};
