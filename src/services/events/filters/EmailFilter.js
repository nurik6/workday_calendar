module.exports = class EmailFilter {
  constructor({ email = null }) {
    this.email = email;
  }

  get() {
    if (this.email !== null) {
      return { email: this.email }
    }
    return null;
  }
};
