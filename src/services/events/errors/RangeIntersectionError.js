const BaseError = require('../../../errors/BaseError');

module.exports = class RangeIntersectionError extends BaseError {
  constructor(message = null) {
    super(message || 'Range intersection error');
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 500;
    this.code = 11;
    this.data = {};
  }
};
