const BaseError = require('../../../errors/BaseError');

module.exports = class AddEventError extends BaseError {
  constructor(message = null) {
    super(message || 'Error was occured while adding new event');
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 500;
    this.code = 5;
    this.data = {};
  }
};
