const BaseError = require('../../../errors/BaseError');

module.exports = class WorkingDayExceeded extends BaseError {
  constructor(message = null, data) {
    super(message || 'Given day is already full.');
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 500;
    this.code = 12;
    this.data = data;
  }
};
