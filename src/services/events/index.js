const _ = require('underscore');
const moment = require('moment');
const NotFoundError = require('../../errors/NotFoundError');
const UniqueIndexError = require('../../errors/UniqueIndexError');
const AddEventError = require('./errors/AddEventError');
const DeleteEventError = require('./errors/DeleteEventError');
const UpdateEventError = require('./errors/UpdateEventError');
const RangeIntersectionError = require('./errors/RangeIntersectionError');
const WorkingDayExceeded = require('./errors/WorkingDayExceeded');

function getDatesByRange(from, to) {
  const fromDate = moment(from);
  const toDate = moment(to);
  const differenceIndays = toDate.diff(fromDate, 'days') + 1;
  return _(differenceIndays).times(n => fromDate.clone().add(n, 'days'));
}

module.exports = class Events {
  constructor(app) {
    this.collection = app.get('db').collection('events');
    this.employees = app.get('db').collection('employees');
    this.attributes = [
      'date',
      'status',
      'note',
      'portion',
      'employee_id',
    ];
  }

  static getStatuses() {
    return [
      'WORK_AT_OFFICE',
      'WORK_REMOTE',
      'SICK_LEAVE',
      'VOCATION',
    ];
  }

  filterByAcceptedAttributes(obj, attributes = null) {
    return _.chain(obj).pick(attributes || this.attributes).omit(_.isNull).value();
  }

  async addOne(event) {
    const acceptedValues = this.filterByAcceptedAttributes(event);
    const defaultValues = {
      portion: event.portion || 8,
      date: event.from,
    };
    const withDefaults = _.defaults(defaultValues, acceptedValues);
    await this.canAddEvent(withDefaults);
    try {
      await this.collection.insertOne(withDefaults);
    } catch (err) {
      if (err.code === 11000) {
        throw new UniqueIndexError('Duplicate key error');
      }
      throw new AddEventError();
    }
  }

  async addRange({ from, to, ...event }) {
    await this.checkRangeIntersection({ from, to, ...event });
    try {
      const filteredEvent = this.filterByAcceptedAttributes(event);
      const events = _.chain(getDatesByRange(from, to))
        .map(date => ({ date: date.toDate(), ...filteredEvent }))
        .map(localEvent => _.defaults({ portion: +process.env.WORK_DAY_DURATION }, localEvent))
        .value();
      await this.collection.insertMany(events);
    } catch (err) {
      if (err.code === 11000) {
        throw new UniqueIndexError('Duplicate key error');
      }
      throw new AddEventError();
    }
  }

  async findById(id) {
    const [result] = await this.collection
      .find({ _id: id })
      .project({ _id: 0 })
      .limit(1)
      .toArray();
    if (!result) {
      throw new NotFoundError('event not found');
    }
    return result;
  }

  async findByDateAndEmployee(date, employeeId) {
    const [result] = await this.collection
      .find({ date, employee_id: employeeId })
      .project({ _id: 0 })
      .limit(1)
      .toArray();
    if (!result) {
      throw new NotFoundError('event not found');
    }
    return result;
  }

  async updateById(id, event) {
    await this.findById(id);
    const acceptedValues = this.filterByAcceptedAttributes(event);
    if (Object.keys(acceptedValues).length === 0) {
      throw new Error('You must provide at least one field');
    }
    try {
      return await this.collection.updateOne({ _id: id }, { $set: acceptedValues });
    } catch (err) {
      throw new UpdateEventError();
    }
  }

  async deleteById(id) {
    await this.findById(id);
    try {
      await this.collection.remove({ _id: id });
    } catch (err) {
      throw new DeleteEventError();
    }
  }

  async allAsStream(filters = {}) {
    const query = [
      { $sort: { date: +process.env.DEFAULT_EVENTS_SORT_ORDER } },
      {
        $lookup: {
          from: 'employees',
          localField: 'employee_id',
          foreignField: '_id',
          as: 'employee',
        },
      },
      {
        $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$employee', 0] }, '$$ROOT'] } },
      },
      { $project: { employee: 0, name: 0, phone: 0 } },
    ];
    if (Object.keys(filters).length > 0) {
      query.push({
        $match: filters,
      });
    }
    const cursor = this.collection
      .aggregate(query, { allowDiskUse: true, batchSize: 10000 });
    return cursor;
  }

  async getStats(filters = {}) {
    const query = [
      { $match: filters },
      {
        $group: {
          _id: {
            group: '$employee_id',
            status: '$status',
          },
          count: { $sum: '$portion' },
        },
      },
      {
        $group: {
          _id: '$_id.group',
          counts: {
            $push: {
              status: '$_id.status',
              workdays: { $divide: ['$count', 8] },
            },
          },
        },
      },
      {
        $lookup: {
          from: 'employees',
          localField: '_id',
          foreignField: '_id',
          as: 'employee',
        },
      },
      {
        $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$employee', 0] }, '$$ROOT'] } },
      },
      { $sort: { email: +process.env.DEFAULT_EVENTS_SORT_ORDER } },
      {
        $project: {
          employee: 0,
          name: 0,
          phone: 0,
        },
      },
    ];
    const result = (await this
      .collection
      .aggregate(query)
      .toArray());
    return result;
  }

  async checkRangeIntersection({ from, to, employee_id }) {
    const daysCount = await this.collection
      .find({ employee_id, date: { $gte: from, $lte: to } })
      .count();
    if (daysCount > 0) {
      throw new RangeIntersectionError();
    }
  }

  async getSumOfHoursByDate(from, employeeId) {
    const totalHours = await this.collection.aggregate([
      {
        $match: { date: from, employee_id: employeeId },
      },
      {
        $group: { _id: null, total: { $sum: '$portion' } },
      },
    ]);
    return totalHours.toArray();
  }

  async canAddEvent(event) {
    const result = await this.getSumOfHoursByDate(event.date, event.employee_id);
    const [{ total: totalDayHours } = { total: 0 }] = result;
    const workDayDuration = +process.env.WORK_DAY_DURATION;
    if (totalDayHours === workDayDuration) {
      throw new WorkingDayExceeded(null, { available: 0 });
    } else if (totalDayHours + event.portion > workDayDuration) {
      throw new WorkingDayExceeded(null, { available: workDayDuration - totalDayHours });
    }
  }

  async canUpdateEvent(oldEvent, newEvent) {
    const result = await this.getSumOfHoursByDate(oldEvent.date, oldEvent.employee_id);
    const [{ total: totalDayHours } = { total: 0 }] = result;
    const workDayDuration = +process.env.WORK_DAY_DURATION;
    if (newEvent.portion + (totalDayHours - oldEvent.portion) > workDayDuration) {
      throw new WorkingDayExceeded(null, { available: workDayDuration - totalDayHours });
    }
  }

  static checkIsOneDate({ from, to }) {
    return moment(to).isSame(from);
  }
};
