const _ = require('underscore');
const AddEmployeeError = require('./errors/AddEmployeeError');
const UpdateEmployeeError = require('./errors/UpdateEmployeeError');
const DeleteEmployeeError = require('./errors/DeleteEmployeeError');
const NotFoundError = require('../../errors/NotFoundError');
const UniqueIndexError = require('../../errors/UniqueIndexError');
const log = require('../../../lib/logger');

module.exports = class Employees {
  constructor(app) {
    this.collection = app.get('db').collection('employees');
    this.attributes = [
      'name',
      'email',
      'phone',
      'note',
    ];
  }

  getAcceptedAttributes(obj) {
    return _.chain(obj).pick(this.attributes).omit(_.isNull).value();
  }

  async add(employee) {
    try {
      const acceptedValues = this.getAcceptedAttributes(employee);
      await this.collection.insertOne(acceptedValues);
    } catch (err) {
      log.error(err);
      if (err.code === 11000) {
        throw new UniqueIndexError('Duplicate key error');
      }
      throw new AddEmployeeError();
    }
  }

  async findByEmail(value) {
    const [result] = await this.collection
      .find({ email: value })
      .project({ _id: 0 })
      .limit(1)
      .toArray();
    if (!result) {
      throw new NotFoundError('Employee not found');
    }
    return result;
  }

  async findById(id) {
    const [result] = await this.collection
      .find({ _id: id })
      .project({ _id: 0 })
      .limit(1)
      .toArray();
    if (!result) {
      throw new NotFoundError('Employee not found');
    }
    return result;
  }

  async updateById(id, employee) {
    await this.findById(id);
    try {
      return await this.collection.updateOne({ _id: id }, { $set: employee });
    } catch (err) {
      log.error(err);
      throw new UpdateEmployeeError();
    }
  }

  async deleteById(id) {
    await this.findById(id);
    try {
      return await this.collection.remove({ _id: id });
    } catch (err) {
      log.error(err);
      throw new DeleteEmployeeError();
    }
  }

  async all() {
    const result = await this.collection.find({}).toArray();
    return result;
  }
};
