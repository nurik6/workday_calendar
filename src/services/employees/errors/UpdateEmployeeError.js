const BaseError = require('../../../errors/BaseError');

module.exports = class UpdateEmployeeError extends BaseError {
  constructor(message = null) {
    super(message || 'Error occured while update employee');
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 500;
    this.code = 6;
    this.data = {};
  }
};
