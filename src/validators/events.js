const moment = require('moment');
const { body, oneOf, query } = require('express-validator/check');
const { isDate, checkId } = require('./commonValidators');
const EventService = require('../services/events');
const EmployeeService = require('../services/employees');

function toDate(value) {
  return moment(value, 'DD.MM.YY HH:mm:ss').toDate();
}

function isBodyParamExists(param) {
  return (value, { req }) => param in req.body;
}

module.exports.create = [
  checkId('employee_id')
    .custom(async (value, { req }) => {
      const service = new EmployeeService(req);
      try {
        await service.findById(value);
      } catch (err) {
        return false;
      }
      return true;
    }).withMessage('employee with such id not found'),
  body('from')
    .not()
    .isEmpty()
    .custom(isDate)
    .withMessage('invalid date format')
    .customSanitizer(toDate),
  body('to')
    .not()
    .isEmpty()
    .custom(isDate)
    .withMessage('invalid date format')
    .custom(isBodyParamExists('from'))
    .withMessage('You must set from param before')
    .custom((value, { req }) => moment(value).isSameOrAfter(req.body.from))
    .withMessage('Must be equal to from or greather')
    .customSanitizer(toDate),
  body('status')
    .not()
    .isEmpty()
    .isIn(EventService.getStatuses()),
  body('portion')
    .not()
    .isEmpty()
    .withMessage('Cannot be empty value')
    .optional()
    .withMessage('Must be integer')
    .isInt({ min: 1, max: 8 })
    .withMessage('min value is 1, max value is 8')
    .toInt()
    .custom((value, { req }) => {
      if ('from' in req.body && 'to' in req.body && 'portion' in req.body) {
        return moment(req.body.to).diff(req.body.from, 'days') === 0;
      }
      return true;
    })
    .withMessage('You can use portion param, only when from and to params are the same date'),
  body('note')
    .not()
    .isEmpty()
    .optional(),
];

module.exports.update = [
  checkId('employee_id')
    .custom(async (value, { req }) => {
      const service = new EmployeeService(req);
      try {
        await service.findById(value);
      } catch (err) {
        return false;
      }
      return true;
    }).withMessage('employee with such id not found'),
  body('date')
    .not()
    .isEmpty()
    .optional()
    .custom(isDate)
    .withMessage('invalid date format')
    .customSanitizer(toDate)
    .custom(async (value, { req }) => {
      const service = new EventService(req);
      if ('employee_id' in req.body) {
        const oldEvent = await service.findByDateAndEmployee(value, req.body.employee_id);
        await service.canUpdateEvent(oldEvent, req.body);
        return true;
      }
      return false;
    }),
  body('status')
    .not()
    .isEmpty()
    .optional()
    .isIn(EventService.getStatuses()),
  body('portion')
    .not()
    .isEmpty()
    .optional()
    .withMessage('Cannot be empty value')
    .withMessage('Must be integer')
    .isInt({ min: 1, max: 8 })
    .withMessage('min value is 1, max value is 8')
    .toInt(),
  body('note')
    .not()
    .isEmpty()
    .optional(),
];

function dateSanitizer(value) {
  const isValidDate = moment(value, process.env.DATE_FORMAT).isValid();
  return isValidDate
    ? moment(value, process.env.DATE_FORMAT).toDate()
    : null;
}

module.exports.list = [
  query('email').toString(),
  query('from')
    .customSanitizer(dateSanitizer),
  query('to')
    .customSanitizer(dateSanitizer),
];

module.exports.stats = [
  query('from')
    .not()
    .isEmpty()
    .customSanitizer(dateSanitizer),
  query('to')
    .not()
    .isEmpty()
    .customSanitizer(dateSanitizer),
];
