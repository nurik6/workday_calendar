const moment = require('moment');
const { ObjectID } = require('mongodb');
const { check } = require('express-validator/check');
const log = require('../../lib/logger');

module.exports.isEmailExists = async (value, { req }) => {
  let employee = [];
  try {
    employee = await req
      .app.get('db')
      .collection('employees')
      .find({ email: value })
      .limit(1)
      .toArray();
  } catch (err) {
    log.error(err);
  }
  return employee.length === 0;
};

module.exports.isDate = value => moment(value, 'DD.MM.YY HH:mm:ss').isValid();

module.exports.checkId = param => check(param)
  .not()
  .isEmpty()
  .withMessage('is required')
  .isMongoId()
  .withMessage('Use valid ObjectID type')
  .customSanitizer(value => ObjectID.isValid(value) && ObjectID(value));
