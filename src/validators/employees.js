const { body } = require('express-validator/check');
const { isEmailExists } = require('./commonValidators');

module.exports.create = [
  body('name')
    .not()
    .isEmpty()
    .withMessage('is required')
    .isLength({ max: 255 })
    .withMessage('max length is 255'),
  body('email')
    .not()
    .isEmpty()
    .withMessage('is required')
    .isEmail()
    .custom(isEmailExists)
    .withMessage('must be unique value'),
  body('phone')
    .not()
    .isEmpty()
    .withMessage('is required')
    .isLength({ max: 50 })
    .withMessage('max length 50'),
  body('note')
    .optional()
    .isLength({ max: 250 })
    .withMessage('max length is 250'),
];

module.exports.update = [
  body('name')
    .optional()
    .isLength({ max: 255 })
    .withMessage('max length is 255'),
  body('email')
    .optional()
    .isEmail()
    .custom(isEmailExists)
    .withMessage('must be unique value'),
  body('phone')
    .optional()
    .isLength({ max: 50 })
    .withMessage('max length 50'),
  body('note')
    .optional()
    .isLength({ max: 250 })
    .withMessage('max length is 250'),
];
