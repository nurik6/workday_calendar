const { Router } = require('express');
const validators = require('../validators/employees');
const commonValidators = require('../validators/commonValidators');

module.exports = (app, controller) => {
  const router = Router();
  // Routes
  router.get('/employee/:id', commonValidators.checkId('id'), controller.show);
  router.post('/employee', validators.create, controller.create);
  router.put('/employee/:id', commonValidators.checkId('id'), validators.update, controller.update);
  router.delete('/employee/:id', commonValidators.checkId('id'), controller.delete);
  router.get('/employees', controller.list);

  app.use('/', router);
};
