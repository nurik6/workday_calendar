const { Router } = require('express');
const validators = require('../validators/events');
const commonValidators = require('../validators/commonValidators');

module.exports = (app, controller) => {
  const router = Router();
  // Routes
  router.get('/event/:id', commonValidators.checkId('id'), controller.show);
  router.get('/events', validators.list, controller.list);
  router.post('/event', validators.create, controller.create);
  router.put('/event/:id', commonValidators.checkId('id'), validators.update, controller.update);
  router.delete('/event/:id', commonValidators.checkId('id'), controller.delete);
  router.get('/events/stats', validators.stats, controller.stats);

  app.use('/', router);
};
