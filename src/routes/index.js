const fs = require('fs');
const _ = require('underscore');
const ControllerHelper = require('../helpers/ControllerHelper');
const asyncMiddleware = require('../middlewares/asyncMiddleware');

module.exports = (app) => {
  fs.readdirSync(__dirname).forEach((file) => {
    if (file === 'index.js') {
      return true;
    }
    const [fileName] = file.split('.');

    const controller = require(
      ControllerHelper.getControllerPath(file)
    );
    const wrappedController = _
      .mapObject(
        controller,
        action => asyncMiddleware(action),
      );
    require(`./${fileName}`)(app, wrappedController);
  });
};
