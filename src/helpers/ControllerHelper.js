const path = require('path');

class ControllerHelper {
  static getName(file) {
    return path.basename(file).split('.')[0];
  }

  static getControllerPath(file) {
    return `../controllers/${this.getName(file)}`;
  }
}

module.exports = ControllerHelper;
