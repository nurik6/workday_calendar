const BaseError = require('./BaseError');

module.exports = class NotFoundError extends BaseError {
  constructor(message, data = {}) {
    super(message);
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 404;
    this.code = 1;
    this.data = data;
  }
};
