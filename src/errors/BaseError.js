module.exports = class BaseError extends Error {
  /**
   * @param {string} message
   */
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.code = 99;
    Error.captureStackTrace(this, this.constructor);
  }
  /**
   * @param {uuid} no
   */

  setErrorNo(no) {
    this.no = no;
  }
};
