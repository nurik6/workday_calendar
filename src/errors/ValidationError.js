const BaseError = require('./BaseError');

module.exports = class ValidationError extends BaseError {
  constructor(message, data = {}) {
    super(message);
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 400;
    this.code = 2;
    this.data = data;
  }
};
