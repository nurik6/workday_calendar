const BaseError = require('./BaseError');

module.exports = class UniqueIndexError extends BaseError {
  constructor(message, data = {}) {
    super(message);
    this.name = this.constructor.name;
    Error.stackTraceLimit = 3;
    Error.captureStackTrace(this, this.constructor);
    this.status = 400;
    this.code = 8;
    this.data = data;
  }
};
