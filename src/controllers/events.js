const { stringify } = require('JSONStream');
const EventsService = require('../services/events');
const { FilterSet } = require('../services/events/filters');
const EmailFilter = require('../services/events/filters/EmailFilter');
const DateRangeFilter = require('../services/events/filters/DateRangeFilter');
const StatsResource = require('../resources/StatsResource');
const ErrorResources = require('../resources/ErrorResource');

module.exports.show = async (req, res) => {
  req.validate();
  const service = new EventsService(req);
  const result = await service.findById(req.params.id);
  res.sendRes(result);
};

module.exports.list = async (req, res) => {
  req.validate();
  const service = new EventsService(req.app);
  const filters = new FilterSet();
  filters.add(new EmailFilter(req.query));
  filters.add(new DateRangeFilter(req.query));
  res.set('Content-Type', 'application/json');
  const stream = await service.allAsStream(filters.get());
  stream.on('error', err => res.sendRes(new ErrorResources(err)));
  res.on('error', (err) => {
    stream.destroy();
    res.sendRes(new ErrorResources(err));
  });
  res.on('close', () => {
    stream.destroy();
    res.send('Response aborted');
  });
  stream
    .pipe(stringify())
    .pipe(res);
};

module.exports.stats = async (req, res) => {
  req.validate();
  const filters = new FilterSet();
  filters.add(new DateRangeFilter(req.query));
  const service = new EventsService(req.app);
  const result = await service.getStats(filters.get());
  res.sendRes(new StatsResource(result));
};

module.exports.create = async (req, res) => {
  req.validate();
  const service = new EventsService(req.app);
  let result;
  if (EventsService.checkIsOneDate(req.body)) {
    result = await service.addOne(req.body);
  } else {
    result = await service.addRange(req.body);
  }
  res.sendRes(result);
};

module.exports.delete = async (req, res) => {
  req.validate();
  const service = new EventsService(req.app);
  const result = await service.deleteById(req.params.id);
  res.sendRes(result);
};

module.exports.update = async (req, res) => {
  req.validate();
  const service = new EventsService(req.app);
  const result = await service.updateById(req.params.id, req.body);
  res.sendRes(result);
};
