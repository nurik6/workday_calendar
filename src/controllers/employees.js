const EmployeesService = require('../services/employees');

module.exports.show = async (req, res) => {
  req.validate();
  const employeeService = new EmployeesService(req.app);
  const employee = await employeeService.findById(req.params.id);
  res.sendRes(employee);
};

module.exports.create = async (req, res) => {
  req.validate();
  const employeeService = new EmployeesService(req.app);
  const result = await employeeService.add(req.body);
  res.sendRes(result);
};

module.exports.update = async (req, res) => {
  req.validate();
  const employeeService = new EmployeesService(req.app);
  const { result: { nModified: affected } } = await employeeService
    .updateById(req.params.id, req.body);
  res.sendRes({ isChanged: affected > 0 });
};

module.exports.delete = async (req, res) => {
  req.validate();
  const employeeService = new EmployeesService(req.app);
  const result = await employeeService.deleteById(req.params.id);
  res.sendRes(result);
};

module.exports.list = async (req, res) => {
  const employeeService = new EmployeesService(req.app);
  const result = await employeeService.all();
  res.sendRes(result);
};
