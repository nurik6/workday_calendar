module.exports = class StatsResource {
  constructor(resultSet) {
    return resultSet.map(({ email, counts }) => {
      function reformatAsObject(acc, { status, workdays }) {
        acc[status] = workdays;
        return acc;
      }
      return { email, byStatus: counts.reduce(reformatAsObject, {}) };
    });
  }
};
