const log = require('../../lib/logger');

module.exports = class ErrorResource {
  constructor(err) {
    if (!(err instanceof Error)) {
      log.error('Error must be instance of Error class');
    }
    const response = {
      code: err.code,
      name: err.name,
      no: err.no || '',
      msg: err.message,
      data: err.data,
    };
    if (process.env.TRACE === '1' && process.env.NODE_ENV === 'development') {
      response.stack = err.stack;
    }
    return response;
  }
};
