
const collectionName = 'events';

module.exports.id = 'create_events_indexes';

module.exports.up = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  this.db.collection(collectionName).ensureIndex('date');
  this.db.collection(collectionName).ensureIndex('status');
  this.db.collection(collectionName).ensureIndex('portion');
  this.db.collection(collectionName).ensureIndex('employee_id');
  done();
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  this.db.collection(collectionName).dropIndexes(['date', 'status', 'portion', 'employee_id']);
  done();
};
