
const collectionName = 'events';
module.exports.id = 'create_events';

module.exports.up = function (done) {
  this.db.createCollection(collectionName, {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        required: ['employee_id'],
        properties: {
          date: {
            bsonType: 'date',
            description: 'must be a date type',
          },
          status: {
            enum: [
              'WORK_AT_OFFICE',
              'WORK_REMOTE',
              'SICK_LEAVE',
              'VOCATION',
            ],
            description: 'can only be one of the enum values and is required',
          },
          note: {
            bsonType: 'string',
          },
          portion: {
            bsonType: 'int',
            description: 'must be integer type',
          },
          employee_id: {
            bsonType: 'objectId',
            description: 'employee_id is required',
          },
        },
      },
    },
  }, done);
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  this.db.collection(collectionName).drop();
  done();
};
