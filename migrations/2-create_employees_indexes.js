
const collectionName = 'employees';

module.exports.id = 'create_employees_indexes';

module.exports.up = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  this.db.collection(collectionName).ensureIndex('email', { unique: true }, done);
};

module.exports.down = function (done) {
  this.db.collection(collectionName).dropIndexes(['email'], done);
  done();
};
