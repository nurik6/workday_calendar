

module.exports.id = 'create_employees';
const collectionName = 'employees';

module.exports.up = function (done) {
  this.db.createCollection(collectionName, {
    validator: {
      $jsonSchema: {
        bsonType: 'object',
        properties: {
          name: { bsonType: 'string' },
          email: { bsonType: 'string' },
          phone: { bsonType: 'string' },
          note: { bsonType: 'string' },
        },
      },
    },
  }, done);
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  this.db.collection(collectionName).drop(done);
};
